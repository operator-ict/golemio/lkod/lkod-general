# Lokální katalog otevřených dat (LKOD)

Lokální katalog otevřených dat (LKOD) slouží primárně k zajištění katalogizace dat v Národním katalogu otevřených dat (NKOD). LKOD je sada webových aplikací díky kterým je možné spravovat a publikovat datové sady do NKODu. LKOD je založen na [DCAT-AP-CZ](https://ofn.gov.cz/rozhran%C3%AD-katalog%C5%AF-otev%C5%99en%C3%BDch-dat/2021-01-11/) a je vyvíjen jako open-source software pod licencí MIT.

## Komponenty

### Katalog

Katalog je webová aplikace, která slouží k prohlížení datových sad v LKODu. Zároveň je katalog možné využít jako informační portál otevřených dat dané instituce, která LKOD provozuje.

Katalog je typicky provozován jako komponenta napojená na LKOD Backend API nebo na SPARQL endpoint.
- LKOD Backend API (doporučeno)
  - od verzí [lkod-catalog v1.2.0](https://gitlab.com/operator-ict/golemio/lkod/lkod-catalog/-/releases/v1.2.0) a [lkod-backend v1.4.5](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/releases/v1.4.5) je možné napojit katalog přímo na LKOD Backend API bez použítí SPARQL endpointu
  - více v [příklady použití](./docs/example_of_usage.md)
- SPARQL endpoint
  - katalog je možné provozovat jako samostatnou komponentu připojenou na SPAQRL endpoint, který obsahuje datové struktury DCAT-AP

Více informací je možné nalézt v repozitáři níže.

- LKOD Catalog:
  - [Kód](https://gitlab.com/operator-ict/golemio/lkod/lkod-catalog)
  - [Changelog](https://gitlab.com/operator-ict/golemio/lkod/lkod-catalog/-/blob/master/CHANGELOG.md)

### Administrace

Administrace se skládá z frontend aplikace a backend API. V frontend aplikaci lze vytvářet a spravovat datové sady. Backend API zajistí, že jsou datové sady nahrány do SPARQL serveru a zároveň jsou vystaveny k harvestaci NKODem.

Od verze [lkod-backend v1.4.5](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/releases/v1.4.5) není nutné používat SPARQL endpoint. Více v [příklady použití](./docs/example_of_usage.md).

Dodržení standardu DCAT-AP-CZ zajišťuje napojení na [oficiální formulář](https://data.gov.cz/formul%C3%A1%C5%99/registrace-datov%C3%A9-sady) Portálu otevřených dat.

Administraci (frontend a backend) je možné provozovat jako samostatnou komponentu. Typicky se provozuje společně s katalogem.

Více informací je možné nalézt v repozitářích níže.

- LKOD Frontend:
  - [Kód](https://gitlab.com/operator-ict/golemio/lkod/lkod-frontend)
  - [Changelog](https://gitlab.com/operator-ict/golemio/lkod/lkod-frontend/-/blob/master/CHANGELOG.md)
- LKOD Backend:
  - [Kód](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend)
  - [Changelog](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/blob/master/CHANGELOG.md)

### SPARQL server

SPARQL server vystavující SPARQL endpoint umožňuje dotazování se nad datovými sadami v LKODu. SPARQL endpoint je využíván pro harvestaci NKODem.

Od verzí [lkod-catalog v1.2.0](https://gitlab.com/operator-ict/golemio/lkod/lkod-catalog/-/releases/v1.2.0) a [lkod-backend v1.4.5](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/releases/v1.4.5) není nutné používat SPARQL endpoint. Více v [příklady použití](./docs/example_of_usage.md).

### Další podpůrné komponenty nebo závislosti

- PostgreSQL databáze
  - Backend API závislost
- Redis databáze
  - Backend API závislost
- FTP server
  - volitelně pro nahrávání distribucí datových sad
- nginx server
  - volitelně (v kominaci s FTP serverem) pro zveřenění distribucí datových sad

## DEMO

Na následujících URL je možné si vyzkoušet jednotlivé komponenty LKODu. Jde zároveň o testovací prostředí, proto je možné, že se v některých případech může chovat jinak než produkční prostředí.

- Katalog
  - https://demo.lkod.cz/
- Admininstrace
  - https://admin-demo.lkod.cz/
    - test@golemio.cz
    - pass
  - API https://api-demo.lkod.cz/
    - OpenAPI docs https://api-demo.lkod.cz/api-docs/
- formulář Portálu otevřených dat
  - https://data.gov.cz/formul%C3%A1%C5%99/registrace-datov%C3%A9-sady

## Instalace

Zprovoznění všech komponent LKODu je popsáno ve složce [demo](demo/README.md). Instalace je popsána pro lokální prostředí, ale je možné ji s mírnými úpravami použít i pro produkční prostředí. Instalace je založena na Dockeru a Docker Compose. Rozběhnutí aplikací mimo Docker je popsáno v repozitářích jednotlivých komponent.

## Architektura LKODu

Zjednodušený diagram architektury LKODu.

![simplified_lkod_architecture.png](./docs/images/simplified_lkod_architecture.png)

### Diagram komunikace při vytváření/editace datové sady

![lkod_sequence.png](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/raw/master/docs/lkod_sequence.png?ref_type=heads)

## Kontakty

V případě jakéhokoliv problému nebo dotazu nás neváhejte kontaktovat pomocí [Issues](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues) nebo emailem na [golemio@operatorict.cz](mailto:golemio@operatorict.cz).

## Podpůrné aplikace

- [CKAN to LKOD migration tool](https://gitlab.com/operator-ict/golemio/lkod/CLM)
- [NKOD in Docker for local testing](https://gitlab.com/operator-ict/golemio/lkod/nkod)
- [LKOD ArcGIS Service](https://gitlab.com/operator-ict/golemio/lkod/lkod-arcgis-service)

## Užitečné návody

- [Příklady použití komponent LKODu](./docs/example_of_usage.md)
- [Návod na instalaci LKODu](./demo/README.md)
- [Registrace organizací do NKOD](./docs/nkod_registration.md)
- [Proč v Golemio nepoužíváme SPARQL?](./docs/why_not_sparql.md)
