# Registrace organizací do NKOD

## Podpora registrace podřízených datových sad do NKODu

Technicky je to vyřešeno pomocí JSON-LD endpointu (`/lod/catalog/?publishers%5B%5D=SLUG_ORGANIZACE`), který splňuje definici [Rozhraní DCAT-AP Dokumenty](https://ofn.gov.cz/rozhran%C3%AD-katalog%C5%AF-otev%C5%99en%C3%BDch-dat/2021-01-11/#dcat-ap-dokumenty). Každá organizace má svůj, jeho URL pak vyplní v registraci lokálního katalogu, kterou odešle ze své datové schránky. Pak se v NKOD zobrazuje jako poskytovatel se svými datovými sadami. Teoreticky tak může jednu instalaci LKODu používat libovolné množství organizací.

## Postup registrace

LKOD registrujte v NKOD pomocí formuláře [Registrace datové sady](https://data.gov.cz/formul%C3%A1%C5%99/registrace-lok%C3%A1ln%C3%ADho-katalogu). Výsledný soubor s registrací odešlete ze své datové schránky.

### Bez SPARQL endpointu nebo každá organizace zvlášť

V poli `Typ API lokálního katalogu` vyberte `DCAT-AP Dokumenty`.

Do pole `URL LKOD API` zadejte url adresu lkod-backendu. Možnosti zadání:

- všechny datové sady dohromady pod jednou organizací
  - `http://lkod-backend:3002/lod/catalog`
- datové sady odděleně podle organizací
  - `http://lkod-backend:3002/lod/catalog?publishers%5B%5D=SLUG_ORGANIZACE`

Kde:
- `http://lkod-backend:3002` je url adresa vašeho lkod-backendu, např. `https://api.opendata.praha.eu`
- `SLUG_ORGANIZACE` je identifikátor organizace, který je součástí url adresy v katalogu otevřených dat v záložce Organizace, např. pro adresu https://opendata.praha.eu/organizations/operator-ict je to `operator-ict`. Dále lze tento údaj najít v administraci lkod-frontend v editaci organizace v poli `URL slug`.

### Se SPARQL endpointem

LKOD registrujte v NKOD pomocí formuláře [Registrace datové sady](https://data.gov.cz/formul%C3%A1%C5%99/registrace-lok%C3%A1ln%C3%ADho-katalogu). Výsledný soubor s registrací odešlete ze své datové schránky.

V poli `Typ API lokálního katalogu` vyberte `DCAT-AP SPARQL Endpoint`.

Do pole `URL LKOD API` zadejte url adresu vašeho SPARQL endpointu.

> Upozornění: Tato varianta nepodporuje zveřejnění datových sad podle organizací. Všechny datové sady jsou zveřejněny dohromady pod jednou organizací.
