# Příklady použití komponent LKODu

V této sekci jsou popsány příklady kombinací komponent LKODu, které jsou možné použít v rámci jedné instance LKODu. Příklady jsou rozděleny podle typu použití.

Seznam komponent:
- LKOD Backend ([lkod-backend](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend))
- LKOD Frontend ([lkod-fronted](https://gitlab.com/operator-ict/golemio/lkod/lkod-frontend))
- Katalog ([lkod-catalog](https://gitlab.com/operator-ict/golemio/lkod/lkod-catalog))
- SPARQL server


## Příklad 1 (nejpopulárnější)

**Máme jednu organizaci nebo více organizací, které si spravují své datové sady. Chceme, aby tyto datové sady byly zveřejněny v NKOD dohromady pod jednou organizací nebo odděleně podle organizací. Chceme datové sady zobrazit i v našem Katalogu otevřených dat. Nepotřebujeme SPARQL endpoint.**

Komponenty:
- lkod-backend
- lkod-frontend
- lkod-catalog

> Upozornění: Tento příklad funguje od verzí [lkod-catalog v1.2.0](https://gitlab.com/operator-ict/golemio/lkod/lkod-catalog/-/releases/v1.2.0) a [lkod-backend v1.4.5](https://gitlab.com/operator-ict/golemio/lkod/lkod-backend/-/releases/v1.4.5).

### Konfigurace

- v `lkod-backend` nastavte env proměnnou `SPARQL_ENABLED=false`, ostatní env `SPARQL_*` nemusíte nastavovat
- v `lkod-catalog` nastavte env proměnnou `DATA_SOURCE="json"` a `ENDPOINT="http://lkod-backend:3002"` (url adresa vašeho lkod-backendu, např. `https://api.opendata.praha.eu`)

### Napojení na NKOD

LKOD registrujte v NKOD pomocí formuláře [Registrace datové sady](https://data.gov.cz/formul%C3%A1%C5%99/registrace-lok%C3%A1ln%C3%ADho-katalogu).

V poli `Typ API lokálního katalogu` vyberte `DCAT-AP Dokumenty`.

Do pole `URL LKOD API` zadejte url adresu lkod-backendu. Možnosti zadání:

- všechny datové sady dohromady pod jednou organizací
  - `http://lkod-backend:3002/lod/catalog`
- datové sady odděleně podle organizací
  - `http://lkod-backend:3002/lod/catalog?publishers%5B%5D=SLUG_ORGANIZACE`

Kde:
- `http://lkod-backend:3002` je url adresa vašeho lkod-backendu, např. `https://api.opendata.praha.eu`
- `SLUG_ORGANIZACE` je identifikátor organizace, který je součástí url adresy v katalogu otevřených dat v záložce Organizace, např. pro adresu https://opendata.praha.eu/organizations/operator-ict je to `operator-ict`. Dále lze tento údaj najít v administraci lkod-frontend v editaci organizace v poli `URL slug`.


## Příklad 2

**Chceme spravovat datové sady a zveřejnit je v NKOD, ale nepotřebujeme náš vlastní Katalog otevřených dat a nepotřebujeme SPARQL endpoint.**

Komponenty:
- lkod-backend
- lkod-frontend

Stejné jako v příkladu 1, ale nepotřebujete komponentu lkod-catalog.


## Příklad 3

**Máme jednu organizaci nebo více organizací, které si spravují své datové sady. Chceme datové sady vystavit přes SPARQL endpoint. Chceme, aby tyto datové sady byly zveřejněny v NKOD dohromady pod jednou organizací nebo odděleně podle organizací. Chceme datové sady zobrazit i v našem Katalogu otevřených dat.**

Komponenty:
- lkod-backend
- lkod-frontend
- lkod-catalog
- SPARQL server

### Konfigurace

- v `lkod-backend` nastavte env proměnnou `SPARQL_ENABLED=true` a vyplňte všechny env `SPARQL_*`
- v `lkod-catalog` nastavte env proměnnou `DATA_SOURCE="sparql"` a `ENDPOINT="http://fuseki:3030"` (url adresa vaší instance SPARQL serveru, např. `https://sparql.opendata.praha.eu`)

### Napojení na NKOD

LKOD registrujte v NKOD pomocí formuláře [Registrace datové sady](https://data.gov.cz/formul%C3%A1%C5%99/registrace-lok%C3%A1ln%C3%ADho-katalogu).

V poli `Typ API lokálního katalogu` vyberte `DCAT-AP SPARQL Endpoint`.

Do pole `URL LKOD API` zadejte url adresu vašeho SPARQL endpointu.

> Upozornění: Tato varianta nepodporuje zveřejnění datových sad podle organizací. Všechny datové sady jsou zveřejněny dohromady pod jednou organizací.
> 
> Pokud chcete zveřejnit datové sady podle organizací, použijte příklad 1.


## Příklad 4

**Máme k dispozici existující SPARQL endpoint a chceme datové sady zobrazit v našem Katalogu otevřených dat.**

Komponenty:
- lkod-frontend

### Konfigurace

- v `lkod-catalog` nastavte env proměnnou `DATA_SOURCE="sparql"` a `ENDPOINT="http://fuseki:3030"` (url adresa vaší instance SPARQL serveru, např. `https://sparql.opendata.praha.eu`)
