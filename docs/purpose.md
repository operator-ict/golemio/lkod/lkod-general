# Cíl
Umožnit uživateli publikovat datavou sadu do LKOD a tím zajistit katalogizaci do Národního katalogu otevřených dat (NKOD). 

## LKOD
Administrace lokálního katalogu otevřených dat umožňuje:
- vytvoření datové sady
- zveřejnění nebo skrytí datové sady 
- úpravu datové sady 
- vyhledat datovou sadu podle filtrů nebo pomocí vyhledávání  

Aplikace zajišťuje kontrolu oprávnění k dané datové sadě. 
LKOD obsahuje pouze metadata. Není to uložiště dat, vizualizační platforma, autorizační autorita ani není odpovědný za kvalitu dat.

LKOD umožňuje publikovat datové sady i do lokálního webového katalogu. Na webovém katalogu lze vyhledávat a filtrovat datové sady podle témat, organizací a klíčových slov.

## Proces publikace
1. Přihlášení a kontrola oprávnění.
1. Zobrazení seznamu dosavadních datových sad.
1. Možnost editovat dosavadní nebo přidat novou. Obě možnosti se liší jen tím, jestli se do formuláře přednačtou data nebo bude prázdný.
1. Formulář pro datovou sadu ([starší verze](https://dev.nkod.opendata.cz/formul%C3%A1%C5%99/registrace-datov%C3%A9-sady), novější pak pouze v [kodu](https://github.com/linkedpipes/dcat-ap-forms)) se ideálně bude používat jako externí knihovna, abychom nemuseli řešit novinky v DCAT-AP. Formulář vygeneruje soubor (json-ld nebo RDF - nevím přesně, ale lze to převádět), který se uloží na lokální úložiště pro editaci v budoucnu a zároveň se odešle datovou schránkou registrace do NKODu. (Odesílat do NKODu je třeba pouze nové datové sady -> další změny si pak NKOD načte z definové URL)
	1. Před odesláním do NKODu je potřeba přidat informace o LKODu, které jsou společné pro všechny datové sady v rámci LKODu.
	1. Zkopírovat access_url do download_url
	1. Dogenerovat unikátní identifikátor datové sady.
1. Výsledné soubory se nahrají jako vstup pro SPARQL endpoint, který slouží k dotazování.

[Seznam SPARQL endpointů a poznámek k nim](https://docs.google.com/document/d/1e8QyiVY06zgH1cmL7cANryUUFtN0TSFgFPfm0r6GEHU/edit#heading=h.toi2e6u0b583)


## Poznámky
- publikace je možné pomocí sady souborů (index + jednotlivé datové sady) nebo pomocí SPARQL endpointu. Chceme používat SPARQL endpoint, protože to redukuje počet dotazů, umožnuje se lépe dotazovat a pracovat s metadaty pomocí API a nikoliv jen procházením souborů.
