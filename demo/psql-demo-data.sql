INSERT INTO "organization" ("name", "identificationNumber", "slug", "logo", "description") VALUES
    ('Operátor ICT, a.s.', '02795281', 'operator-ict', 'https://rabingolemio.blob.core.windows.net/rabin-lkod/sparql/organizations/oict.svg', 'Operátor ICT, a.s. je městskou společností, která pro Hlavní město Praha primárně zajišťuje agendu a řízení projektů Smart City, odborné poradenství v oblasti ICT a realizaci ICT projektů pro městské části a další městské společnosti. Společnost Operátor ICT, a.s. vznikla přejmenováním společnosti Operátor OPENCARD, a.s., která byla založena přijetím Stanov společnosti jediným zakladatelem, Hlavním městem Prahou, a to v souladu s ustanovením § 8 a § 250 zákona o obchodních korporacích a § 59 zákona o Hlavním městě Praze na zasedání zastupitelstva Hlavního města Prahy dne 27. 2. 2014.');

INSERT INTO "user" ("email", "password", "name", "role", "hasDefaultPassword") VALUES
    ('test@golemio.cz', '$2b$10$PCAwdjhktHpwOBJBW7RIFeGD8IYXiSMIohRdG83hl8j.lerlWOwP.', 'test user', 'user', false);

INSERT INTO "user_organization" ("userId", "organizationId") VALUES
    ((SELECT id FROM "user" WHERE "email" = 'test@golemio.cz'), (SELECT id FROM "organization" WHERE "slug" = 'operator-ict'));

INSERT INTO "dataset" ("id", "userId", "status", "createdAt", "updatedAt", "data", "organizationId", "validationResult") VALUES
    ('b5ba0f4d-0298-43f4-b344-1bdbd3eb4073', (SELECT id FROM "user" WHERE "email" = 'test@golemio.cz'), 'unpublished', '2022-08-05 16:13:36.349+02', '2023-01-17 12:50:17.359+01', '{"iri": "http://lkod-backend:3002/lod/catalog/b5ba0f4d-0298-43f4-b344-1bdbd3eb4073", "typ": "Datová sada", "popis": {"cs": "sada pro testování", "en": "dataset for some testing"}, "téma": ["http://publications.europa.eu/resource/authority/data-theme/TECH", "http://publications.europa.eu/resource/authority/data-theme/SOCI"], "název": {"cs": "testovací sada", "en": "testing dataset"}, "@context": "https://ofn.gov.cz/rozhraní-katalogů-otevřených-dat/2021-01-11/kontexty/rozhraní-katalogů-otevřených-dat.jsonld", "distribuce": [{"iri": "http://lkod-backend:3002/lod/catalog/b5ba0f4d-0298-43f4-b344-1bdbd3eb4073/distributions/csv-test.csv", "typ": "Distribuce", "název": {"cs": "testovací csv soubor", "en": "test csv file"}, "formát": "http://publications.europa.eu/resource/authority/file-type/CSV", "schéma": "http://lkod-ftp-public:3010/lkod/operator-ict/b5ba0f4d-0298-43f4-b344-1bdbd3eb4073/test.csv", "typ_média": "http://www.iana.org/assignments/media-types/text/csv", "podmínky_užití": {"typ": "Specifikace podmínek užití", "osobní_údaje": "https://data.gov.cz/podmínky-užití/neobsahuje-osobní-údaje/", "autorské_dílo": "https://data.gov.cz/podmínky-užití/neobsahuje-autorská-díla/", "databáze_jako_autorské_dílo": "https://data.gov.cz/podmínky-užití/není-autorskoprávně-chráněnou-databází/", "databáze_chráněná_zvláštními_právy": "https://data.gov.cz/podmínky-užití/není-chráněna-zvláštním-právem-pořizovatele-databáze/"}, "přístupové_url": "http://lkod-ftp-public:3010/lkod/operator-ict/b5ba0f4d-0298-43f4-b344-1bdbd3eb4073/test.csv", "soubor_ke_stažení": "http://lkod-ftp-public:3010/lkod/operator-ict/b5ba0f4d-0298-43f4-b344-1bdbd3eb4073/test.csv", "typ_média_komprese": "http://www.iana.org/assignments/media-types/application/gzip", "typ_média_balíčku": "http://www.iana.org/assignments/media-types/application/gzip"}], "dokumentace": "https://data.gov.cz/datov%C3%A9-sady?t%C3%A9mata=http%3A%2F%2Feurovoc.europa.eu%2F6121", "specifikace": ["https://data.gov.cz/datov%C3%A9-sady?t%C3%A9mata=http%3A%2F%2Feurovoc.europa.eu%2F6121"], "poskytovatel": "http://lkod-backend:3002/organization/operator-ict", "prvek_rúian": ["https://linked.cuzk.cz/resource/ruian/stat/1"], "kontaktní_bod": {"typ": "Organizace", "e-mail": "mailto:john@doe.com", "jméno": {"cs": "John Doe"}}, "koncept_euroVoc": ["http://eurovoc.europa.eu/3928"], "klíčové_slovo": {"cs": ["test", "testovací"], "en": ["test", "testing"]}, "časové_pokrytí": {"typ": "Časový interval", "konec": "2022-08-17", "začátek": "2022-08-05"}, "geografické_území": ["http://publications.europa.eu/resource/authority/place/CZE_PRG"], "prostorové_pokrytí": [], "časové_rozlišení": "P7D", "periodicita_aktualizace": "http://publications.europa.eu/resource/authority/frequency/WEEKLY", "prostorové_rozlišení_v_metrech": "5"}', (SELECT id FROM "organization" WHERE "slug" = 'operator-ict'), '{"status":"hasWarnings","messages":["data/iri musí vyhovět regulárnímu výrazu \"^https://\"","data/distribuce/0/přístupové_url musí vyhovět regulárnímu výrazu \"^https://\""]}');

-- only for storage
CREATE EXTENSION pgcrypto;

CREATE TABLE "pureftpd_users" (
    "username" text NOT NULL,
    "password" text NOT NULL,
    "home_dir" text NOT NULL,
    "quota_size" integer DEFAULT '500' NOT NULL,
    CONSTRAINT "pureftpd_users_pkey" PRIMARY KEY ("username")
) WITH (oids = false);
COMMENT ON COLUMN "pureftpd_users"."quota_size" IS 'MB';

INSERT INTO "pureftpd_users" ("username", "password", "home_dir", "quota_size") 
VALUES ('lkod', crypt('pass', gen_salt('bf')), '/home/public/lkod', '500');
